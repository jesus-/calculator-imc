package com.example.appimc

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.RadioButton
import android.widget.Toast
import com.google.android.material.slider.RangeSlider
import com.google.android.material.slider.Slider


class MainActivity : AppCompatActivity() {

    var radiob1:RadioButton?=null
    var radiob2:RadioButton?=null
    private lateinit var materialSlider :RangeSlider
    private lateinit var materialSlider2 :RangeSlider

    private var altura: Double = 0.0
    private var peso: Double = 0.0

    private var imc: Double=0.0
    private var imcfinal: Double= 0.0



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //relacionar las variables creadas con el id de cada radiobutotn
        radiob1=findViewById(R.id.radiob1)
        radiob2=findViewById(R.id.radiob2)

        //Relacionar el id del button
        val btncalc = findViewById<Button>(R.id.btn1)

        /*
               //referenciar el material slider
               val rngSlider = findViewById<RangeSlider>(R.id.slider1)
               val rngSlider2 = findViewById<RangeSlider>(R.id.slider2)



               rngSlider.addOnChangeListener { slider, _, _ ->
                   // El valor actual del MaterialRangeSlider ha cambiado
                   // Puedes utilizar 'slider.values' para obtener los valores seleccionados
                   val selectedValues = slider.values
                   val minValue = selectedValues[0]
                   val maxValue = selectedValues[1]
                   // Realiza acciones con los valores seleccionados
               }

               rngSlider2.addOnChangeListener { slider, _, _ ->
                   // El valor actual del MaterialRangeSlider ha cambiado
                   // Puedes utilizar 'slider.values' para obtener los valores seleccionados
                   val selectedValues = slider.values

                   // Realiza acciones con los valores seleccionados

               }
                */


        materialSlider = findViewById(R.id.slider1)
        materialSlider2 = findViewById(R.id.slider2)


        materialSlider.addOnChangeListener { _, value, _ ->
            // El valor del MaterialSlider ha cambiado
            peso = value.toDouble()
            // Se almacena el valor en una variable Double (peso)
        }

        materialSlider2.addOnChangeListener { _, value, _ ->
            // El valor del MaterialSlider ha cambiado
            altura = value.toDouble()
            // Se almacena el valor en una variable Double (altura)
        }

        btncalc.setOnClickListener{
            //Llamar a la funcion que calcula el IMC

            calcularImc()
            calcular()


            //abrir el activity 1
            val intent = Intent(this, Activity1::class.java)
            intent.putExtra("IMC_EXTRA", imc)
            startActivity(intent)

        }




    }

    private fun calcular (){
        var cad:String="Seleccionado: \n"

        if (radiob1?.isChecked==true){
            cad += "opcion#1 \n"
        }

        if (radiob2?.isChecked==true){
            cad += "opcion#2 \n"
        }
        Toast.makeText(this, "$cad", Toast.LENGTH_SHORT).show()
    }

    private fun calcularImc ():Double {
        imc = peso/(altura*altura)
        Toast.makeText(this, "$imc", Toast.LENGTH_SHORT).show()
        //intent.putExtra("IMC_EXTRA", imc)

        return imc
    }

}