package com.example.appimc

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import kotlin.math.roundToInt

class Activity1 : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_1)

        // Recupera el valor del IMC del Intent
        val imc = intent.getDoubleExtra("IMC_EXTRA", 0.0)

        val redondeado = imc.roundToInt()



        // Ahora puedes utilizar el valor del IMC en ResultActivity
        // Por ejemplo, mostrarlo en un TextView
        val textView = findViewById<TextView>(R.id.tvRes2)
        textView.text = "Tu IMC es: $redondeado"
    }
}